//
//  UIView.swift
//  eval
//
//  Created by hengchengfei on 15/9/1.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit
import JGProgressHUD

public extension UIView {
    public var width:CGFloat! {
        get {
           return self.frame.width
        }
        set(newValue){
            var frame = self.frame
            frame.size.width = newValue
            self.frame = frame
        }
    }
    
    public var height:CGFloat! {
        get {
            return self.frame.height
        }
        set(newValue){
            var frame = self.frame
            frame.size.height = newValue
            self.frame = frame
        }
    }
    
    public var x:CGFloat! {
        get {
            return self.frame.origin.x
        }
        set(newValue){
            var frame = self.frame
            frame.origin.x = newValue
            self.frame = frame
        }
    }
    
    public var y:CGFloat! {
        get {
            return self.frame.origin.y
        }
        set(newValue){
            var frame = self.frame
            frame.origin.y = newValue
            self.frame = frame
        }
    }
    
    /**
      默认在底部显示弹出消息
    */
    public func showMessage(msg:String){
        let hud = JGProgressHUD(style: JGProgressHUDStyle.Dark)
        hud.indicatorView = nil
        hud.position = JGProgressHUDPosition.BottomCenter
        hud.textLabel.text = msg
//      hud.backgroundColor = UIColor(netHex: 0x000000, alpha: 0.3) // 背景色
        hud.interactionType = JGProgressHUDInteractionType.BlockTouchesOnHUDView //阻塞背后所有的触摸行为
        hud.marginInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0) //距底部距离
        hud.showInView(self)
        hud.dismissAfterDelay(2.0)
    }
    
    /**
      显示加载
    */
    public func showProgress(msg:String? = nil ,style:JGProgressHUDStyle? = .Dark){
        let hud = JGProgressHUD(style: style!)
        hud.textLabel.text = msg
//        hud.backgroundColor = UIColor(netHex: 0x000000, alpha: 0.3) // 背景色
        hud.interactionType = JGProgressHUDInteractionType.BlockAllTouches //阻塞背后所有的触摸行为
        
        hud.showInView(self, animated: true)
    }
    
    /**
      显示成功信息
    */
    public func showSuccess(msg:String? = nil,style:JGProgressHUDStyle = .ExtraLight){
        let hud = JGProgressHUD(style: style)
        hud.indicatorView = JGProgressHUDImageIndicatorView(image: UIImage(named: "success"))
        if let m = msg {
            hud.textLabel.text = m
        }
        hud.interactionType = JGProgressHUDInteractionType.BlockAllTouches //阻塞背后所有的触摸行为
        hud.showInView(self)
        hud.dismissAfterDelay(1.5)
    }
    /**
     显示错误信息
     */
    public func showError(msg:String? = nil,style:JGProgressHUDStyle = .ExtraLight){
        let hud = JGProgressHUD(style: style)
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        if let m = msg {
            hud.textLabel.text = m
        }
        hud.interactionType = JGProgressHUDInteractionType.BlockAllTouches //阻塞背后所有的触摸行为
        hud.showInView(self)
        hud.dismissAfterDelay(1.5)
    }
    
    /**
     隐藏所有弹出消息
    */
    public func hiddenAllMessage(){
        for v in self.subviews {
            if v.isKindOfClass(JGProgressHUD) {
                v.removeFromSuperview()
            }
        }
    }
}