//
//  HotModel.swift
//  IOSTemplate
//
//  Created by hengchengfei on 15/11/25.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit
import ObjectMapper

public class HotModel: Mappable {
    // 用户id
    public var userId:Int64?
    
    // 用户名
    public var userName:String?
    
    // 用户头像
    public var userIcon:String?
    
    // 评论时间
    public var commentTime:Double?
    
    // 评论内容
    public var commentContent:String?
    
    public init(){
        
    }
    
    required public init?(_ map: Map) {
        
    }
    
    public func mapping(map: Map) {
        userId <- (map["user_id"], TransformOf<Int64, Int>(fromJSON: { $0.map { Int64($0) } }, toJSON: { $0.map { Int($0) } }))
        userName <- map["user_name"]
        userIcon <- map["user_icon"]
        commentTime <- map["comment_time"]
        commentContent <- map["comment_content"]
    }
}
